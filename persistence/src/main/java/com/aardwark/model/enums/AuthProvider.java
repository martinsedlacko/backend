package com.aardwark.model.enums;

public enum AuthProvider {
    local,
    google
}
