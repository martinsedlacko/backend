package com.aardwark.repository;

import com.aardwark.model.ActivityEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.sql.Date;
import java.util.Optional;

public interface ActivityRepository extends JpaRepository<ActivityEntity, Long> {
    Optional<ActivityEntity> findByUserIdAndDate(Long userId, Date date);
}
