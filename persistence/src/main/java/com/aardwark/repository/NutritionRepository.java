package com.aardwark.repository;

import com.aardwark.model.NutritionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.Optional;

@Repository
public interface NutritionRepository extends JpaRepository<NutritionEntity, Long> {
    Optional<NutritionEntity> findByUserIdAndDate(Long userId, Date date);
}
