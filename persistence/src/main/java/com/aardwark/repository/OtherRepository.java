package com.aardwark.repository;

import com.aardwark.model.OtherEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.sql.Date;
import java.util.Optional;

public interface OtherRepository extends JpaRepository<OtherEntity, Long> {
    Optional<OtherEntity> findByUserIdAndDate(Long userId, Date date);
}
