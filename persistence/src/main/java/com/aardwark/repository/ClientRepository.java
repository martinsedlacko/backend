package com.aardwark.repository;

import com.aardwark.model.ClientEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClientRepository extends JpaRepository<ClientEntity, Long> {
    List<ClientEntity> findAllByTrainer_Id(Long trainerId);
    Page<ClientEntity> findAllByTrainerId(Long trainerId, Pageable pageable);
}
