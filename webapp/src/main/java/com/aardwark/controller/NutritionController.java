package com.aardwark.controller;

import com.aardwark.error.CommonErrorEnum;
import com.aardwark.exception.NotFoundException;
import com.aardwark.model.NutritionEntity;
import com.aardwark.model.UserEntity;
import com.aardwark.model.dto.NutritionDto;
import com.aardwark.model.dto.NutritionRequestDto;
import com.aardwark.model.mapper.NutritionMapper;
import com.aardwark.service.NutritionService;
import com.aardwark.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/nutrition")
@Slf4j
public class NutritionController {

    private final NutritionService nutritionService;
    private final NutritionMapper nutritionMapper;
    private final UserService userService;

    public NutritionController(final NutritionService nutritionService,
                               final ModelMapper modelMapper,
                               final UserService userService) {
        this.nutritionService = nutritionService;
        this.nutritionMapper = new NutritionMapper(modelMapper);
        this.userService = userService;
    }

    @PostMapping(value = "/findByDateAndUser", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<NutritionDto> getByUserIdAndDate(@RequestBody NutritionRequestDto request){
        log.debug("Get NutritionEntity by UserId: {} and Date: {}", request.getUserId(), request.getDate());
        Optional<NutritionEntity> entity = this.nutritionService
                .findByUserIdAndDate(request.getUserId(), request.getDate());
        NutritionDto result;

        if(entity.isPresent())
            result = nutritionMapper.convertToDto(entity.get());
        else
            result = new NutritionDto();
        log.info("Returning Nutrition with ID : " + (entity.isPresent() ? entity.get().getId() : "Not found"));
        return ResponseEntity.ok(result);
    }

    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<NutritionEntity> save(@RequestBody NutritionDto dto){
        log.debug("Save NutritionEntity by UserId: {}", dto.getUserId());
        UserEntity user = this.userService.findById(dto.getUserId())
                .orElseThrow(() -> new NotFoundException(CommonErrorEnum.NOT_FOUND, "User not found"));
        NutritionEntity entity = this.nutritionMapper.convertToEntity(dto, user);
        entity = this.nutritionService.save(entity);
        log.debug("Save NutritionEntity by UserId: {} success. NutritionEntity Id : {}",
                dto.getUserId(), entity.getId());

        return ResponseEntity.ok(entity);
    }
}
