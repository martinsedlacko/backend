package com.aardwark.controller;

import com.aardwark.model.ClientEntity;
import com.aardwark.model.UserEntity;
import com.aardwark.payload.ApiResponse;
import com.aardwark.service.ClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/client")
@Slf4j
public class ClientController {

    private final ClientService clientService;

    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping("/findAllByTrainerId/{trainerId}")
    public ResponseEntity<List<UserEntity>> findAllByTrainerId(@PathVariable Long trainerId){
        List<ClientEntity> clients = this.clientService.findAllByTrainerId(trainerId);
        List<UserEntity> users = clients.stream()
                .map(ClientEntity::getTrainer)
                        .collect(Collectors.toList());
        log.info("Returning list of clients with size {}. Trainer ID: {}", clients.size(), trainerId);
        return ResponseEntity.ok(users);
    }

    @GetMapping("/findAllByTrainerId/{trainerId}/{pageSize}/{offset}")
    public ResponseEntity<Page<ClientEntity>> findAllByTrainerIdPageable(@PathVariable Long trainerId,
                                                                                    @PathVariable int pageSize,
                                                                                    @PathVariable int offset){
        Page<ClientEntity> clients = this.clientService.
                findAllByTrainerIdPageable(trainerId, PageRequest.of(offset, pageSize));
        log.info("Returning page of clients with totalElements: {}, totalPages: {}",
                clients.getTotalElements(), clients.getTotalPages());
        return ResponseEntity.ok(clients);
    }
}
