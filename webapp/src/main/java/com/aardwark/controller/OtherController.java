package com.aardwark.controller;

import com.aardwark.error.CommonErrorEnum;
import com.aardwark.exception.NotFoundException;
import com.aardwark.model.OtherEntity;
import com.aardwark.model.UserEntity;
import com.aardwark.model.dto.OtherDto;
import com.aardwark.model.dto.OtherRequestDto;
import com.aardwark.model.mapper.OtherMapper;
import com.aardwark.payload.ApiResponse;
import com.aardwark.service.OtherService;
import com.aardwark.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/other")
@Slf4j
public class OtherController {

    private final OtherService otherService;
    private final OtherMapper otherMapper;
    private final UserService userService;

    public OtherController(OtherService otherService, ModelMapper modelMapper, UserService userService) {
        this.otherService = otherService;
        this.otherMapper = new OtherMapper(modelMapper);
        this.userService = userService;
    }

    @PostMapping(value = "/findByUserAndDate", produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OtherDto> getByUserAndDate(@RequestBody OtherRequestDto request){
        log.debug("Get OtherEntity by UserId: {} and Date: {}", request.getUserId(), request.getDate());
        Optional<OtherEntity> entity = this.otherService
                .findByUserIdAndDate(request.getUserId(), request.getDate());
        OtherDto payload;

        if(entity.isPresent()){
            payload = this.otherMapper.convertToDto(entity.get());
        }else {
            payload = new OtherDto();
        }
        log.debug("Returning OtherEntity with ID : " + (entity.isPresent() ? entity.get().getId() : "Not found"));
        return ResponseEntity.ok(payload);
    }

    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OtherEntity> save(@RequestBody OtherDto dto) {
        log.debug("Save OtherEntity by UserId: {}", dto.getUserId());
        UserEntity user = this.userService.findById(dto.getUserId()).orElseThrow(
                () -> new NotFoundException(CommonErrorEnum.NOT_FOUND, "User was not found")
        );
        OtherEntity entity = this.otherMapper.convertToEntity(dto, user);
        entity = this.otherService.save(entity);
        log.debug("Save OtherEntity by UserId: {} success. OtherEntity Id : {}", dto.getUserId(), entity.getId());
        return new ResponseEntity<>(entity, HttpStatus.CREATED);
    }
}
