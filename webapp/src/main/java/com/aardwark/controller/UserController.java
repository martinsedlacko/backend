package com.aardwark.controller;

import com.aardwark.error.CommonErrorEnum;
import com.aardwark.exception.NotFoundException;
import com.aardwark.exception.ResourceNotFoundException;
import com.aardwark.model.UserEntity;
import com.aardwark.repository.UserRepository;
import com.aardwark.security.oauth2.CurrentUser;
import com.aardwark.security.oauth2.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    private final UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/user/me")
    @PreAuthorize("hasRole('USER')")
    public UserEntity getCurrentUser(@CurrentUser UserPrincipal userPrincipal) {
        return userRepository.findById(userPrincipal.getId())
                .orElseThrow(() -> new NotFoundException(CommonErrorEnum.NOT_FOUND, "User was not found"));
    }
}
