package com.aardwark.controller;

import com.aardwark.error.CommonErrorEnum;
import com.aardwark.exception.NotFoundException;
import com.aardwark.model.ActivityEntity;
import com.aardwark.model.UserEntity;
import com.aardwark.model.dto.ActivityDto;
import com.aardwark.model.dto.ActivityRequestDto;
import com.aardwark.model.mapper.ActivityMapper;
import com.aardwark.service.ActivityService;
import com.aardwark.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/activity")
@Slf4j
public class ActivityController {

    private final ActivityService activityService;
    private final ActivityMapper activityMapper;
    private final UserService userService;

    public ActivityController(final ActivityService activityService,
                              final UserService userService,
                              final ModelMapper modelMapper) {
        this.activityService = activityService;
        this.activityMapper = new ActivityMapper(modelMapper);
        this.userService = userService;
    }

    @PostMapping(value = "/findByUserAndDate", produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ActivityDto> getByUserIdAndActivity(@RequestBody ActivityRequestDto request){
        log.debug("Get ActivityEntity by UserId: {} and Date: {}", request.getUserId(), request.getDate());
        Optional<ActivityEntity> entity = this.activityService.findByUserIdAndDate(request.getUserId(),
                request.getDate());
        ActivityDto result;

        if(entity.isPresent()){
            result = activityMapper.convertToDto(entity.get());
        }else {
            result  = new ActivityDto();
        }
        log.info("Returning Activity with ID : " + (entity.isPresent() ? entity.get().getId() : "Not found"));
        return ResponseEntity.ok(result);
    }

    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ActivityEntity> save(@RequestBody ActivityDto dto){
        UserEntity user = this.userService.findById(dto.getUserId())
                .orElseThrow(() -> new NotFoundException(CommonErrorEnum.NOT_FOUND, "User not found"));
        ActivityEntity entity = this.activityMapper.convertToEntity(dto, user);
        entity = this.activityService.save(entity);
        log.debug("Save ActivityEntity by UserId: {} success. ActivityEntity Id : {}",
                dto.getUserId(), entity.getId());

        return ResponseEntity.ok(entity);
    }
}
