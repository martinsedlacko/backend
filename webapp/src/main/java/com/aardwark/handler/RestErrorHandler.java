package com.aardwark.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.Locale;

import static com.aardwark.error.CommonErrorEnum.COMMON_ERROR_CODE_PREFIX;

@ControllerAdvice
@Slf4j
public class RestErrorHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(
            final Exception ex,
            final Object body,
            final HttpHeaders headers,
            final HttpStatus status,
            final WebRequest request) {
        final String errorCode = COMMON_ERROR_CODE_PREFIX + status.name().toLowerCase(Locale.ENGLISH)
                .replace('_', '.');
        final ErrorResponse errorResponse = new ErrorResponse(ex.getMessage(), errorCode, LocalDateTime.now());
        log.error("Exception occurred {}", errorResponse, ex);
        return new ResponseEntity<>(errorResponse, status);
    }
}
