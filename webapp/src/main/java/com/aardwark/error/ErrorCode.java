package com.aardwark.error;

public interface ErrorCode {

    String getCode();

    String getDescription();
}
