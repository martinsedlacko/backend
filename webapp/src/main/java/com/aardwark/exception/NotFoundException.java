package com.aardwark.exception;

import com.aardwark.error.ErrorCode;

public class NotFoundException extends GeneralException{

    public NotFoundException(ErrorCode error, String message) {
        super(error, message);
    }
}
