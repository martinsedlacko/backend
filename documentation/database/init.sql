CREATE TABLE nutritions (
    id bigint auto_increment primary key,
    protein int,
    carbohydrates int,
    fat int,
    calories int,
    fiber int,
    user_id bigint,
    date date,
    FOREIGN KEY (user_id) REFERENCES users(id),
    UNIQUE KEY `nutritions_unique_key` (`date`, `user_id`)
);

CREATE TABLE activity (
  id bigint auto_increment primary key,
  steps int,
  exercise int,
  user_id  bigint,
  date date,
  FOREIGN KEY (user_id) REFERENCES users(id),
  UNIQUE KEY `activity_unique_key` (`date`,`user_id`)
);

CREATE TABLE other (
   id bigint auto_increment primary key,
   sleep double,
   user_id bigint,
   date date,
   foreign key (user_id) references users(id),
   unique key `other_unique_key` (`date`, `user_id`)
)

CREATE TABLE role (
    id bigint auto_increment primary key,
    code varchar(64)
)

CREATE TABLE user_role (
    user_id bigint,
    role_id bigint,
    foreign key (user_id) references users(id),
    foreign key (role_id) references role(id)
)

CREATE TABLE client (
    id bigint auto_increment primary key,
    user_id bigint,
    trainer_id bigint,
    FOREIGN KEY (user_id) REFERENCES users(id),
    FOREIGN KEY (trainer_id) REFERENCES users(id)
);
