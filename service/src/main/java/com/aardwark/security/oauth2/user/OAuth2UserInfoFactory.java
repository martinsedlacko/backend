package com.aardwark.security.oauth2.user;

import com.aardwark.exception.OAuth2AuthenticationProcessingException;
import com.aardwark.model.enums.AuthProvider;

import java.util.Map;

public class OAuth2UserInfoFactory {

    /**
     * In future, facebok, github ... login can be added
     */
    public static OAuth2UserInfo getOAuth2UserInfo(String registrationId, Map<String, Object> attributes){
        if(registrationId.equalsIgnoreCase(AuthProvider.google.toString())){
            return new GoogleOAuth2UserInfo(attributes);
        }else {
            throw new OAuth2AuthenticationProcessingException("Sorry! Login with " + registrationId + " is not supported yet.");
        }
    }
}
