package com.aardwark.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class OtherDto {
    private Long id;
    private double sleep;
    private Long userId;
    private Date date;
}
