package com.aardwark.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;

@Getter
@AllArgsConstructor
@Setter
@NoArgsConstructor
public class NutritionDto {

    private Long id;
    private int protein;
    private int carbohydrates;
    private int fat;
    private int fiber;
    private int calories;
    private Date date;
    private Long userId;
}
