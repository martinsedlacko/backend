package com.aardwark.model.dto;

import lombok.Getter;

import java.sql.Date;

@Getter
public class ActivityRequestDto {
    private Long userId;
    private Date date;
}
