package com.aardwark.model.dto;

import lombok.Getter;

import java.sql.Date;

@Getter
public class NutritionRequestDto {

    private Long userId;
    private Date date;
}
