package com.aardwark.model.mapper;

import com.aardwark.model.NutritionEntity;
import com.aardwark.model.UserEntity;
import com.aardwark.model.dto.NutritionDto;
import com.aardwark.service.UserService;
import org.modelmapper.ModelMapper;

public class NutritionMapper {

    private final ModelMapper modelMapper;

    public NutritionMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public NutritionEntity convertToEntity(NutritionDto dto, UserEntity user){
        NutritionEntity entity = this.modelMapper.map(dto, NutritionEntity.class);
        entity.setUser(user);
        return entity;
    }

    public NutritionDto convertToDto(final NutritionEntity entity) {
        NutritionDto dto = this.modelMapper.map(entity, NutritionDto.class);
        dto.setUserId(entity.getUser().getId());
        return dto;
    }
}
