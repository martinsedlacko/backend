package com.aardwark.model.mapper;

import com.aardwark.model.ActivityEntity;
import com.aardwark.model.UserEntity;
import com.aardwark.model.dto.ActivityDto;
import org.modelmapper.ModelMapper;

public class ActivityMapper {

    private final ModelMapper modelMapper;

    public ActivityMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public ActivityEntity convertToEntity(ActivityDto dto, UserEntity user){
        ActivityEntity activityEntity = modelMapper.map(dto, ActivityEntity.class);
        activityEntity.setUser(user);
        return activityEntity;
    }

    public ActivityDto convertToDto(ActivityEntity entity) {
        ActivityDto dto = this.modelMapper.map(entity, ActivityDto.class);
        dto.setUserId(entity.getUser().getId());
        return dto;
    }
}
