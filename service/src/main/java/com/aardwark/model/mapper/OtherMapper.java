package com.aardwark.model.mapper;

import com.aardwark.model.OtherEntity;
import com.aardwark.model.UserEntity;
import com.aardwark.model.dto.OtherDto;
import org.modelmapper.ModelMapper;

public class OtherMapper {
    private final ModelMapper modelMapper;

    public OtherMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public OtherDto convertToDto(OtherEntity entity) {
        return new OtherDto(entity.getId(), entity.getSleep(), entity.getUser().getId(), entity.getDate());
    }

    public OtherEntity convertToEntity(OtherDto dto, UserEntity user){
        OtherEntity entity = modelMapper.map(dto, OtherEntity.class);
        entity.setUser(user);
        return entity;
    }
}
