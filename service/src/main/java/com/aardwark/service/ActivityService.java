package com.aardwark.service;

import com.aardwark.model.ActivityEntity;
import com.aardwark.repository.ActivityRepository;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.Optional;

@Service
public class ActivityService {

    private final ActivityRepository repository;

    public ActivityService(ActivityRepository repository) {
        this.repository = repository;
    }

    public Optional<ActivityEntity> findByUserIdAndDate(Long userId, Date date){
        return this.repository.findByUserIdAndDate(userId, date);
    }

    public ActivityEntity save(ActivityEntity entity){
        return this.repository.save(entity);
    }
}
