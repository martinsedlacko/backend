package com.aardwark.service;

import com.aardwark.model.UserEntity;
import com.aardwark.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {
    private final UserRepository repository;

    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    public Optional<UserEntity> findById(Long id) {
        return repository.findById(id);
    }
}
