package com.aardwark.service;

import com.aardwark.model.ClientEntity;
import com.aardwark.repository.ClientRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientService {
    private final ClientRepository repository;

    public ClientService(ClientRepository repository) {
        this.repository = repository;
    }

    public List<ClientEntity> findAllByTrainerId(Long trainerId) {
        return this.repository.findAllByTrainer_Id(trainerId);
    }

    public Page<ClientEntity> findAllByTrainerIdPageable(Long trainerId, Pageable page) {
        return this.repository.findAllByTrainerId(trainerId, page);
    }
}
