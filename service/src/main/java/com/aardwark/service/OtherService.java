package com.aardwark.service;

import com.aardwark.model.OtherEntity;
import com.aardwark.repository.OtherRepository;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.Optional;

@Service
public class OtherService {
    private final OtherRepository repository;

    public OtherService(OtherRepository repository) {
        this.repository = repository;
    }

    public Optional<OtherEntity> findByUserIdAndDate(Long userId, Date date){
        return this.repository.findByUserIdAndDate(userId, date);
    }

    public OtherEntity save(OtherEntity entity) {
        return this.repository.save(entity);
    }
}
