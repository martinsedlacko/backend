package com.aardwark.service;

import com.aardwark.model.NutritionEntity;
import com.aardwark.repository.NutritionRepository;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.Optional;

@Service
public class NutritionService {

    private final NutritionRepository repository;

    public NutritionService(NutritionRepository repository) {
        this.repository = repository;
    }

    public Optional<NutritionEntity> findByUserIdAndDate(Long userId, Date date) {
        return this.repository.findByUserIdAndDate(userId, date);
    }

    public NutritionEntity save(NutritionEntity entity) {
        return this.repository.save(entity);
    }
}
